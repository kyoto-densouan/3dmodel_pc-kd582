# README #  
  
1/3スケールのNEC製PC-KD582風小物のstlファイルです。  
スイッチ類、I/Oポート等は省略しています。  
  
組み合わせたファイルとパーツごとに配置したファイルの二種類があります。  
元ファイルはAUTODESK 123D DESIGNです。  

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-kd582/raw/035ffc61b24f436768d6bdd763a4c3d421d813db/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-kd582/raw/035ffc61b24f436768d6bdd763a4c3d421d813db/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-kd582/raw/035ffc61b24f436768d6bdd763a4c3d421d813db/ExampleImage.png)
